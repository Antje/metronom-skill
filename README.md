# Metronom Skill für Amazon Echo
wissenschaftliches Projekt von Antje Rumpf

<p align="center"><img width="200" src="./assets/MetronomSkillSymbol.png"/></p>


### Beschreibung

Ein Metronom (Taktgeber) für den Amazon Echo, der mit der Sprache gesteuert werden kann

### Grundfunktion

#### Der Skill muss folgende Punkte enthalten

- [x] Anschlag in Schleife gespielt
- [x] 1/4 Takt (default)
- [x] 20 - 240 bpm (Geschwindigkeit)
- [x] einzelne Note (default)
- [x] start, pause, aus, Hilfe
- [x] Spiele einen Takt mit 80 bpm
- [x] schneller (+10 bpm), langsamer (-10 bpm), viel schneller (+20 bpm), erhöhe um .. bpm
- [x] Information zu aktuellen Parametern

#### Technisch bereits umgesetzt

- [x] Audiointerface des SDKs implementiert
- [x] Wert in Datenbank gespeichert
- [x] Session in Datenbank übernehmen
- [x] Aktueller Bug: Wert wird nicht in Datenbank übernommen
- [x] Session pausieren und fortsetzen können
- [x] 20.mp3 funktioniert nicht
- [x] Überflüssige Audio Intents löschen
- [x] InMetronomSession false setzen
- [x] min max Antwort prüfen 


### Voraussetzungen
- npm (https://www.npmjs.com/get-npm)
- ask cli (https://developer.amazon.com/de/docs/smapi/quick-start-alexa-skills-kit-command-line-interface.html)
- Amazon Developer Account (https://developer.amazon.com/alexa/console/ask)
- AWS Account (https://console.aws.amazon.com/console/home?region=us-east-1) 

### Installation

- `cd lambda/custom; npm install;`

- `npm install -g ask-cli` installieren und mit Amazon Developer Account konfigurieren
- AWS IAM user anlegen mit Berechtigungen für Lambda und DynamoDB
- `ask deploy` ausführen um Skill-Projekt zu erstellen und Lambda-Funktion anzulegen.
