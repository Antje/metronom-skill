/* CONSTANTS */

// Informationen zum Skill-Projekt in Alexa Konsole 
// https://developer.amazon.com/alexa/console/ask 
module.exports.skill = {
    appId: 'amzn1.ask.skill.dab9203d-9f03-4995-b299-27a925142b71',
    dynamoDBTableName: 'Metronom',
};

// Metronom Parameter
module.exports.audioData = {
    urlStart: 'https://s3-eu-west-1.amazonaws.com/arumpf-metronom-skill/metronom-sound/',
    urlEnd: '.mp3',
    unit: 'B P M',
    minValue: 20,
    maxValue: 240
}

// Metronom Sprache
// großgeschriebene Wörter müssen durch Variable ersetzt werden.
module.exports.phrases = {
    startMetronom: [
        "Los geht's mit TEMPO " + exports.audioData.unit,
        "Metronom mit TEMPO " + exports.audioData.unit + " startet jetzt.",
        "TEMPO " + exports.audioData.unit + ". Hier bitte."
    ],
    endMetronom: [
        "Auf Wiederhören",
        "Bis bald."
    ],
    tempoAsk: "Auf wie viele " + exports.audioData.unit + " soll ich das Metronom einstellen?",
    tempoReprompt: "Nenne ein Tempo. Zum Beispiel Achtzig " + exports.audioData.unit,
    noRunningMetronom: 'Es wurde noch kein Metronom gestartet. Sage "Alexa, starte Metronom" um den Skill zu benutzen'
}