// HELPERS

module.exports = {
    /**
     * Gibt eine zufällige Phrase aus Array zurück
     * @param {[String]} phraseArray: Phrasen-Array aus dem zufällig gewählt wird
     */
    getRandromPhrase: function (phraseArray) {
        const randomIndex = Math.floor((Math.random() * phraseArray.length));
        return phraseArray[randomIndex]
    },

    /**
     * Prüft ob Slot-Werte valide sind und liefert Objekt zurück
     * @param {any} filledSlots: Request Intent-Slots
     */
    getSlotValues: function (filledSlots) {
        const slotValues = {};

        if (filledSlots) {
            console.log(`The filled slots: ${JSON.stringify(filledSlots)}`);
            Object.keys(filledSlots).forEach((item) => {
                const name = filledSlots[item].name;

                if (filledSlots[item] &&
                    filledSlots[item].resolutions &&
                    filledSlots[item].resolutions.resolutionsPerAuthority[0] &&
                    filledSlots[item].resolutions.resolutionsPerAuthority[0].status &&
                    filledSlots[item].resolutions.resolutionsPerAuthority[0].status.code) {
                    switch (filledSlots[item].resolutions.resolutionsPerAuthority[0].status.code) {
                        case 'ER_SUCCESS_MATCH':
                            console.log('FILLED SLOTS A:')
                            slotValues[name] = {
                                id: filledSlots[item].resolutions.resolutionsPerAuthority[0].values[0].value.id,
                                synonym: filledSlots[item].value,
                                resolved: filledSlots[item].resolutions.resolutionsPerAuthority[0].values[0].value.name,
                                isValidated: true,
                            };
                            break;
                        case 'ER_SUCCESS_NO_MATCH':
                            console.log('FILLED SLOTS B:')
                            slotValues[name] = {
                                synonym: filledSlots[item].value,
                                resolved: filledSlots[item].value,
                                isValidated: false,
                            };
                            break;
                        default:
                            break;
                    }
                } else {
                    console.log('FILLED SLOTS C:')
                    slotValues[name] = {
                        synonym: filledSlots[item].value,
                        resolved: filledSlots[item].value,
                        isValidated: false,
                    };
                }
            }, this);
        }
        return slotValues;
    },

    adjustTempo: function (tempo) {
        if (tempo % 5 != 0) {
            // tempo Datei noch nicht vorhanden
            // wenn Rest größer 2 aufrunden sonst abrunden
            if (tempo % 5 < 3) {
                tempo = Math.floor(tempo / 5) * 5;
            } else {
                tempo = Math.ceil(tempo / 5) * 5;
            }
        }
        return tempo
    },

    getErrorResponse: function (handlerInput) {
        return handlerInput.responseBuilder
            .speak('Entschuldige, versuche es erneut')
            .getResponse()
    }
}
