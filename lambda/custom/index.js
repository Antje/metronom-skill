const alexa = require("ask-sdk");

// Skill-Projekt Infos und Sprachdateien ausgelagert.
const constants = require("./constants");
// Hilfsfunktionen ausgelagert
const helpers = require("./helpers");

// Intent Handlers ---------

/**
 * Funktion wird aufgerufen, wenn Skill gestartet wird
 */
const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest'
    },
    async handle(handlerInput) {
        // Parameter aus Datenbank abfragen wenn vorhanden
        const metronomInfo = await getMetronomInfo(handlerInput);
        let answer;
        let reprompt;

        if (!metronomInfo.hasPreviousMetronomSession) { // DB-Wert vorhanden
            answer = 'Willkommen. ' + constants.phrases.tempoAsk;
            reprompt = constants.phrases.tempoReprompt;
        } else { // kein DB-Wert vorhanden
            metronomInfo.inMetronomSession = false;
            answer = `Die Geschwindigkeit des Metronoms war <say-as interpret-as="cardinal">${metronomInfo.tempo}</say-as> ${constants.audioData.unit}. Möchtest du den Takt fortsetzen?`
            reprompt = 'Du kannst Ja sagen um mit gleichem Tempo fortzufahren oder nein um die Geschwindigkeit zu ändern.'
        }

        return handlerInput.responseBuilder
            .speak(answer) // Wird direkt ausgesprochen von Alexa
            .reprompt(reprompt) // Falls keine Antwort gegeben wird, wird Reprompt Alexa
            .getResponse(); // auf Antwort warten
    }
}

/**
 * AudioPlayer Requests verarbeiten
 * https://developer.amazon.com/de/docs/custom-skills/audioplayer-interface-reference.html#playbackstarted
 */
const AudioPlayerEventHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type.startsWith('AudioPlayer.');
    },
    async handle(handlerInput) {
        const {
            requestEnvelope,
            attributesManager,
            responseBuilder
        } = handlerInput;
        const audioPlayerEventName = requestEnvelope.request.type.split('.')[1];
        const {
            metronomInfo
        } = await attributesManager.getPersistentAttributes();


        switch (audioPlayerEventName) {
            /**
             * Sent when Alexa begins playing the audio stream previously sent in a Play directive. This lets your skill verify that playback began successfully.
             */
            case 'PlaybackStarted':
                console.log('PlaybackStarted')
                // Datenbank-Parameter setzen
                metronomInfo.inMetronomSession = true;
                metronomInfo.hasPreviousMetronomSession = true;
                break;
            /**
             * Sent when the stream Alexa is playing comes to an end on its own.
             */
            case 'PlaybackFinished':
                console.log('PlaybackFinished')
                metronomInfo.inMetronomSession = false
                break;
            /**
             * Sent when Alexa stops playing an audio stream in response to a voice request or an AudioPlayer directive.
             */
            case 'PlaybackStopped':
                console.log('PlaybackStopped')
                break;
            /**
             * Sent when the currently playing stream is nearly complete and the device is ready to receive a new stream.
             */
            case 'PlaybackNearlyFinished':
                console.log('PlaybackNearlyFinished')
                controller.repeat(handlerInput)
                break;
            /**
             * Sent when Alexa encounters an error when attempting to play a stream.
             */
            case 'PlaybackFailed':
                metronomInfo.inMetronomSession = false;
                console.log('Playback Failed : %j', handlerInput.requestEnvelope.request.error);
                break;
            default:
                throw new Error('Should never reach here!');
        }
        return responseBuilder.getResponse()
    }
};

/**
 * Überprüfen ob das AudioPlayer Interface verwendet werden kann
 */
const CheckAudioInterfaceHandler = {
    async canHandle(handlerInput) {
        const audioPlayerInterface = ((((handlerInput.requestEnvelope.context || {}).System || {}).device || {}).supportedInterfaces || {}).AudioPlayer;
        return audioPlayerInterface === undefined
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak('Entschuldige, dieser Skill unterstützt das Gerät nicht.')
            .withShouldEndSession(true)
            .getResponse();
    },
};

/**
 * Metronom starten; Custom Intent
 */
const StartMetronomHandler = {
    async canHandle(handlerInput) {

        // Metronomparameter aus Datenbank abrufen
        const metronomInfo = await getMetronomInfo(handlerInput);
        const request = handlerInput.requestEnvelope.request;

        // Wenn noch keine Session vorhanden ist, 
        if (!metronomInfo.inMetronomSession) {
            return request.type === 'IntentRequest' && request.intent.name === 'StartMetronomIntent';
        }
        if (request.type === 'PlaybackController.PlayCommandIssued') {
            return true;
        }

        // Nach Pause wieder fortsetzen
        if (request.type === 'IntentRequest') {
            return request.intent.name === 'StartMetronomIntent' || request.intent.name === 'AMAZON.ResumeIntent';
        }
    },
    handle(handlerInput) {
        console.log('StartMetronomHandler')
        const filledSlots = handlerInput.requestEnvelope.request.intent.slots;
        let tempo;
        if (typeof filledSlots !== 'undefined') {
            const slotValues = helpers.getSlotValues(filledSlots)

            tempo = slotValues.tempo.synonym
            if (tempo == "?") {
                return helpers.getErrorResponse(handlerInput)
            }
        }
        return controller.play(handlerInput, tempo)
    }
};


/**
 * Metronom Geschwindigkeit anpassen
 */
const ChangeTempoHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest' && (request.intent.name === 'ChangeTempoIntent');
    },
    async handle(handlerInput) {
        const metronomInfo = await getMetronomInfo(handlerInput);
        if (metronomInfo.inMetronomSession) {
            console.log('ChangeTempoIntent')
            const filledSlots = handlerInput.requestEnvelope.request.intent.slots;
            if (typeof filledSlots !== 'undefined') {
                const slotValues = helpers.getSlotValues(filledSlots)
                let direction = slotValues.direction.id
                let adjective = typeof slotValues.adjectivePace.id !== 'undefined' ? slotValues.adjectivePace.id : ""

                let newTempo;
                switch (`${adjective} ${direction}`) {
                    case " faster":
                        newTempo = parseInt(metronomInfo.tempo) + 5
                        break;
                    case "much faster":
                        newTempo = parseInt(metronomInfo.tempo) + 10
                        break;
                    case "muchMore faster":
                        newTempo = parseInt(metronomInfo.tempo) + 20
                        break;
                    case " slower":
                        newTempo = parseInt(metronomInfo.tempo) - 5
                        break;
                    case "much slower":
                        newTempo = parseInt(metronomInfo.tempo) - 10
                        break;
                    case "muchMore slower":
                        newTempo = parseInt(metronomInfo.tempo) - 20
                        break;
                    default:
                        console.log(`default switch - kontrollieren für: ${adjective} ${direction}`)
                        return helpers.getErrorResponse(handlerInput)
                }
                metronomInfo.tempoChanged = true

                return controller.play(handlerInput, newTempo)
            }
        }
        return handlerInput.responseBuilder
            .speak(constants.phrases.noRunningMetronom)
            .getResponse()
    }
}

const ChangeTempoToHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request
        return request.type === 'IntentRequest' && request.intent.name === 'ChangeTempoToIntent'
    },
    async handle(handlerInput) {
        console.log("ChangeTempoToHandler")

        const filledSlots = handlerInput.requestEnvelope.request.intent.slots;
        let tempo;
        if (typeof filledSlots !== 'undefined') {
            const slotValues = helpers.getSlotValues(filledSlots)
            tempo = slotValues.tempo.synonym
            if (tempo == "?") {
                return helpers.getErrorResponse(handlerInput)
            }
        } else {
            return helpers.getErrorResponse(handlerInput)
        }

        const metronomInfo = await getMetronomInfo(handlerInput);
        metronomInfo.tempoChanged = true
        return controller.play(handlerInput, tempo)
    }
}
const ChangeTempoByHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request
        return request.type === 'IntentRequest' && request.intent.name === 'ChangeTempoByIntent'
    },
    async handle(handlerInput) {
        console.log('ChangeTempoByHandler')

        const filledSlots = handlerInput.requestEnvelope.request.intent.slots;
        let newTempo;
        let metronomInfo = await getMetronomInfo(handlerInput)
        let tempo = metronomInfo.tempo
        if (typeof filledSlots !== 'undefined') {
            const slotValues = helpers.getSlotValues(filledSlots)
            const direction = slotValues.direction.id

            if (slotValues.tempo.synonym == "?" || parseInt(slotValues.tempo.synonym) == NaN) {
                return helpers.getErrorResponse(handlerInput)
            }

            if (direction == "faster") {
                newTempo = parseInt(slotValues.tempo.synonym) + parseInt(tempo)
            } else if (direction == "slower") {
                newTempo = parseInt(tempo) - parseInt(slotValues.tempo.synonym)
            } else {
                console.log(`ChangeTempoByHandler: ${direction}`)
                return helpers.getErrorResponse(handlerInput)
            }
            metronomInfo.tempoChanged = true
            return controller.play(handlerInput, newTempo)
        } else {
            return helpers.getErrorResponse(handlerInput)
        }
    }
}

const GetTempoHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' && request.intent.name === 'GetTempoIntent';
    },
    async handle(handlerInput) {
        console.log('GetTempoHandler')
        let metronomInfo = await getMetronomInfo(handlerInput);
        let cardText;
        let speakText;
        if (metronomInfo.inMetronomSession) {
            cardText = `Das aktuelle Tempo ist ${metronomInfo.tempo} bpm`
            speakText = 'Das aktuelle Tempo ist <say-as interpret-as="cardinal">' + metronomInfo.tempo + '</say-as> ' + constants.audioData.unit

        } else if (metronomInfo.tempo !== 0) {
            cardText = `Das aktuelle Tempo ist ${metronomInfo.tempo} bpm`
            speakText = 'Das Metronom ist nicht aktiv. Ich habe mir <say-as interpret-as="cardinal">' + metronomInfo.tempo + '</say-as> ' + constants.audioData.unit + ' beim letzten mal gemerkt.'
        } else {
            cardText = ''
            speakText = constants.phrases.noRunningMetronom
        }
        return handlerInput.responseBuilder
            .speak(speakText)
            .withSimpleCard(cardText, cardText)
            .getResponse()
    }
}

const PausePlaybackHandler = {
    async canHandle(handlerInput) {
        const metronomInfo = await getMetronomInfo(handlerInput);
        const request = handlerInput.requestEnvelope.request;

        return metronomInfo.inMetronomSession && request.type === 'IntentRequest' && (request.intent.name === 'AMAZON.StopIntent' || request.intent.name === 'AMAZON.CancelIntent' || request.intent.name === 'AMAZON.PauseIntent');
    },
    handle(handlerInput) {
        return controller.stop(handlerInput)
    }
};

const YesHandler = {
    async canHandle(handlerInput) {
        const metronomInfo = await getMetronomInfo(handlerInput);
        const request = handlerInput.requestEnvelope.request;

        return !metronomInfo.inMetronomSession && request.type === 'IntentRequest' && request.intent.name === 'AMAZON.YesIntent';
    },
    handle(handlerInput) {
        return controller.play(handlerInput);
    },
};

const NoHandler = {
    async canHandle(handlerInput) {
        const metronomInfo = await getMetronomInfo(handlerInput);
        const request = handlerInput.requestEnvelope.request;

        return !metronomInfo.inMetronomSession && request.type === 'IntentRequest' && request.intent.name === 'AMAZON.NoIntent';
    },
    async handle(handlerInput) {
        const metronomInfo = await getMetronomInfo(handlerInput);

        metronomInfo.tempoChanged = true;
        metronomInfo.hasPreviousMetronomSession = false;

        return handlerInput.responseBuilder
            .speak(constants.phrases.tempoAsk)
            .reprompt(constants.phrases.tempoReprompt)
            .getResponse()
    },
};

const HelpHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest' && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    async handle(handlerInput) {
        const metronomInfo = await getMetronomInfo(handlerInput);
        let message;

        if (!metronomInfo.hasPreviousMetronomSession) {
            message = 'Du kannst ein Metronom starten indem du sagst "starte Metronom mit einer Geschwindigkeit von Hundertzwanzig Schlägen pro Minute"';
        } else if (!metronomInfo.inMetronomSession) {
            message = `Aktuell hat das Metronom das Tempo <say-as interpret-as="cardinal">${metronomInfo.tempo}</say-as> ${constants.audioData.unit}. Möchtest du einen Metronom mit dieser Geschwindigkeit starten?`;
        } else {
            message = 'Du kannst ein Metronom, einen Taktgeber, starten indem du sagst "starte Metronom mit einer Geschwindigkeit von Hundertzwanzig Schlägen pro Minute." und die Geschwindigkeit ändern indem du "Alexa, frage Metronom ob es schneller spielen kann." sagst';
        }

        return handlerInput.responseBuilder
            .speak(message)
            .reprompt(message)
            .getResponse();
    },
};

const ExitHandler = {
    async canHandle(handlerInput) {
        const metronomInfo = await getMetronomInfo(handlerInput);
        const request = handlerInput.requestEnvelope.request;


        return !metronomInfo.inMetronomSession && request.type === 'IntentRequest' && (request.intent.name === 'AMAZON.StopIntent' || request.intent.name === 'AMAZON.CancelIntent');
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak(helpers.getRandromPhrase(constants.phrases.endMetronom))
            .getResponse();
    },
};

const SystemExceptionHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'System.ExceptionEncountered';
    },
    handle(handlerInput) {
        console.log(`System exception encountered: ${handlerInput.requestEnvelope.request.reason}`);
    },
};

const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

        return handlerInput.responseBuilder.getResponse();
    },
};
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`Error handled: ${error.message}`);
        const message = 'Entschuldige, diese Funktion wird nicht unterstützt. Bitte sag Hilfe um zu erfahren, was du sagen kannst.';

        return handlerInput.responseBuilder
            .speak(message)
            .getResponse();
    },
};

/* INTERCEPTORS */

const LoadPersistentAttributesRequestInterceptor = {
    async process(handlerInput) {
        const persistentAttributes = await handlerInput.attributesManager.getPersistentAttributes();

        // Check if user is invoking the skill the first time and initialize preset values
        if (Object.keys(persistentAttributes).length === 0) {
            handlerInput.attributesManager.setPersistentAttributes({
                metronomInfo: {
                    tempo: 0,
                    inMetronomSession: false,
                    hasPreviousMetronomSession: false,
                    tempoChanged: false
                },
            });
        }
    },
};

const SavePersistentAttributesResponseInterceptor = {
    async process(handlerInput) {
        await handlerInput.attributesManager.savePersistentAttributes();
    },
};

// ------ Controller -------

const controller = {
    async play(handlerInput, tempo) {
        const {
            responseBuilder
        } = handlerInput;

        if (tempo == null || typeof tempo === 'undefined') {
            const metronomInfo = await getMetronomInfo(handlerInput);
            tempo = metronomInfo.tempo
        }

        if (tempo) {
            if (tempo < constants.audioData.minValue) {
                // tempo zu langsam
                responseBuilder
                    .speak(`Das mindest Tempo ist <say-as interpret-as="cardinal">${constants.audioData.minValue}</say-as>. ${tempo} ${constants.audioData.unit} ist zu langsam. Wie viele ${constants.audioData.unit} soll ich einstellen?`)
                    .reprompt(constants.phrases.tempoAsk)
            } else if (tempo > constants.audioData.maxValue) {
                // tempo zu schnell
                responseBuilder
                    .speak(`Das maximal Tempo ist <say-as interpret-as="cardinal">${constants.audioData.maxValue}</say-as>. ${tempo} ${constants.audioData.unit} ist zu schnell. Wie viele ${constants.audioData.unit} soll ich einstellen?`)
                    .reprompt(constants.phrases.tempoAsk)
            } else {
                const metronomInfo = await getMetronomInfo(handlerInput);

                metronomInfo.tempo = helpers.adjustTempo(tempo);

                const url = constants.audioData.urlStart + metronomInfo.tempo + constants.audioData.urlEnd;
                const answer = helpers.getRandromPhrase(constants.phrases.startMetronom).replace('TEMPO', metronomInfo.tempo)

                responseBuilder
                    .speak(answer)
                    .addDirective({
                        type: 'AudioPlayer.Play',
                        playBehavior: 'REPLACE_ALL',
                        audioItem: {
                            stream: {
                                token: '0',
                                url: url,
                                offsetInMilliseconds: 0
                            }
                        }
                    })

                if (await canThrowCard(handlerInput)) {
                    const cardContent = `Metronom: ${metronomInfo.tempo} bpm`;
                    responseBuilder.withSimpleCard(cardContent, cardContent);
                }
            }
        }

        return responseBuilder.getResponse();
    },
    stop(handlerInput) {
        return handlerInput.responseBuilder
            .speak('Gestoppt. Tempo gespeichert.')
            .addAudioPlayerStopDirective()
            .getResponse();
    },
    async repeat(handlerInput) {
        const {
            responseBuilder
        } = handlerInput;

        const metronomInfo = await getMetronomInfo(handlerInput);

        const url = constants.audioData.urlStart + metronomInfo.tempo + constants.audioData.urlEnd;
        return responseBuilder.addAudioPlayerPlayDirective(
            'ENQUEUE',
            url,
            '0',
            0,
            '0',
        );
    }
}

// ---- Async Helpers

async function getMetronomInfo(handlerInput) {
    const attributes = await handlerInput.attributesManager.getPersistentAttributes();
    return attributes.metronomInfo;
}

async function canThrowCard(handlerInput) {
    const {
        requestEnvelope,
    } = handlerInput;
    const metronomInfo = await getMetronomInfo(handlerInput);

    if (requestEnvelope.request.type === 'IntentRequest' && metronomInfo.tempoChanged) {
        metronomInfo.tempoChanged = false;
        return true;
    }
    return false;
}

async function changeTempo(handlerInput, newTempo) {
    const metronomInfo = await getMetronomInfo(handlerInput);
    metronomInfo.tempo = newTempo;
}

// ------ Skill Builder -------

const skillBuilder = alexa.SkillBuilders.standard();
exports.handler = skillBuilder
    .addRequestHandlers(
        CheckAudioInterfaceHandler,
        LaunchRequestHandler,
        StartMetronomHandler,
        ChangeTempoHandler,
        ChangeTempoToHandler,
        ChangeTempoByHandler,
        GetTempoHandler,
        HelpHandler,
        SystemExceptionHandler,
        SessionEndedRequestHandler,
        ExitHandler,
        AudioPlayerEventHandler,
        PausePlaybackHandler,
        YesHandler,
        NoHandler
    )
    .addRequestInterceptors(LoadPersistentAttributesRequestInterceptor)
    .addResponseInterceptors(SavePersistentAttributesResponseInterceptor)
    .addErrorHandlers(ErrorHandler)
    .withAutoCreateTable(true)
    .withTableName(constants.skill.dynamoDBTableName)
    .withSkillId(constants.skill.appId)
    .lambda()