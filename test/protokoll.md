# Testing Protokoll

Testprotokoll für den Metronom Skill

## Voraussetzungen:

- Amazon Echo
- kein Datenbankeintrag für Benutzer

## Test 1: Skill starten mit allen Parametern

- -> Alexa, starte Metronom mit einer Geschwindigkeit von 120 bpm
- <- Los geht's mit 120 bpm. \*Sound wird abgespielt
- -> Alexa, aus.
- <- Auf Wiederhören \*Tempo in Datenbank speichern, Sound stoppt

## Test 2: Skill starten ohne alle Parameter und Grenzen testen

- -> Alexa, öffne Metronom
- <- Willkommen, auf wie viele bpm soll ich das Metronom einstellen?
- -> Auf 10
- <- Das mindest Tempo ist 20. 10 bpm ist zu langsam.
- -> auf 330 bpm
- <- Das maximal Tempo ist 240 bpm. 330 bpm ist zu schnell.
- -> auf 30 bpm
- <- Metronom mit 30 bpm startet jetzt. \*Sound wird abgespielt
- -> Alexa frage Metronom wie schnell es eingestellt ist. 
- <- Das aktuelle Tempo ist 30 bpm \* Sound wird für die Aussage automatisch leiser
- -> Alexa, aus.
- <- Bis bald.

## Test 3: Wert bei nicht aktiver Session abfragen TODO
- -> Alexa, frage Metronom welche Geschwindigkeit es spielt. 
- <- Das Metronom ist nicht aktiv. Ich habe mir 30 bpm beim letzten mal gemerkt.
- oder
- <- Es wurde noch kein Metronom gestartet. Sage "Alexa, starte Metronom" um den Skill zu benutzen. 

## Test 4: Geschwindigkeit erhöhen ohne konkretes Tempo
- -> Alexa, starte Metronom mit einer Geschwindigkeit von 120 bpm
- <- Los geht's mit 120 bpm. \*Sound wird abgespielt
<!-- - -> Alexa stelle Metronom schneller -->
- -> Alexa erhöhe die Geschwindigkeit vom Metronom?
- <- Weiter geht's mit 125 bpm \*Sound wird abgespielt
- -> Alexa frage Metronom ob es die Geschwindigkeit erhöhen kann
- <- Weiter geht's mit 135 bpm \*Sound wird abgespielt

## Test 5: Geschwindigkeit erhöhen mit konkretem Tempo
- -> Alexa, starte Metronom mit einer Geschwindigkeit von 120 bpm
- <- Los geht's mit 120 bpm. \*Sound wird abgespielt

## Test 6: Um Hilfe bitten
- -> Alexa, starte Metronom
- <- Willkommen, auf wie viele bpm soll ich das Metronom einstellen?
- -> Hilfe
- <- Du kannst ein Metronom starten indem du sagst "starte Metronom mit Hundertzwanzig Schlägen pro Minute"

## Test 7: Falsche Antwort geben
- -> Alexa, starte Metronom 
- <- Willkommen, auf wie viele bpm soll ich das Metronom einstellen? 
- -> Elefant
- <- 


## Test 8: Aus nach Wiederholung 
- Alexa, starte metronom
- alte session aufrufen? 
- nein antworten
- wie viele? 
- aus

## Test 9: Geschwindigkeit verändern
- starte metronom mit einer Geschwindigkeit von 80 bpm
- -> Los geht's mit 80 bpm
- <- Alexa frage metronom ob es die Geschwindigkeit stark erhöhen kann.
- -> Metronom mit 90 bpm startet jetzt. 
- <- Alexa frage metronom ob es die Geschwindigkeit sehr stark verringern kann.
- -> Los geht's mit 70 bpm
- <- Alexa frage metronom ob es die Geschwindigkeit um 30 erhöhen kann.
- -> Metronom mit 100 bpm gestartet
- <- Alexa frage metronom ob es das Tempo auf 200 bpm ändern kann. 
- -> Los geht's mit 200 bpm